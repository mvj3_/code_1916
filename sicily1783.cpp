//题目网址: 
//http://soj.me/1783

//题目分析:
//用STL的优先队列做，相当简单(默认你知道什么叫优先队列)
//遇到非0数字就压入队列中，遇到0就将队列所有元素弹出。
//用时0.02s，属于中上一点的水平

#include <iostream>
#include <string>
#include <queue>;

using namespace std;

int main()
{
    int t;
    string ans;
    string num;
    priority_queue<char> que;

    cin >> t;
    while (t--) {
        cin >> num;

        for (int i = 0; i < num.length(); i++) {
            if (num[i] == '0') {
                while (!que.empty()) {
                    ans += que.top();
                    que.pop();
                }
                ans += num[i];
            } else {
                que.push(num[i]);
            }
        }

        while (!que.empty()) {
            ans += que.top();
            que.pop();
        }
        cout << ans << endl;
        ans.clear();
    }
    
    return 0;
}                                 